#include <stdio.h>
#include <stdlib.h>

#include "TreeNode.h"

//function which prints a node and it's features
void printNode(TreeNode* node) {
	printf("I am printing %d\n", node->value);
	
	if (hasLeft(node))
		printf("my left child is %d\n", ((TreeNode*)node->left)->value);
	if (hasRight(node))
		printf("my right child is %d\n", ((TreeNode*)node->right)->value);
	if (node->parent)
		printf("my parent is %d\n", ((TreeNode*)node->parent)->value);
	
	printf("\n");
}

int main() {

	TreeNode * head = 0, *temp;
	head = insert(&head,5);

	insert(&head, 3);
	insert(&head, 10);
	insert(&head, 7);
	temp = insert(&head, 6);
	insert(&head, -8);
	insert(&head, 9);

	removeNode(head);

	postOrder(temp, &printNode);
	destroyTree(temp);
	
	return 0;
}

