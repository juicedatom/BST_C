#include <stdlib.h>
#include <stdio.h>
#include "TreeNode.h"

int hasLeft(TreeNode * node) {
	return node->left!=0;
}

int hasRight(TreeNode * node) {
	return node->right!=0;
}

int numberOfChildren(TreeNode * node) {
	return hasLeft(node) + hasRight(node);
}

void destroyTree(TreeNode * node) {
	TreeNode * parent = node->parent;
	if (parent) {
		if (parent->left == node)
			parent->left = NULL;
		else
			parent->right = NULL;
	}
	void (*fctn)(TreeNode*) = (void (*)(TreeNode*)) &free;
	postOrder(node, fctn);
}

void postOrder(TreeNode * node, void (*fctn)(TreeNode*)) {
	if (!node) return;

	if (hasLeft(node))
		postOrder(node->left, fctn);
	
	if (hasRight(node))
		postOrder(node->right, fctn);
		
	(*fctn)(node);
}

void preOrder(TreeNode * node, void (*fctn)(TreeNode*)) {
	if (!node) return;

	(*fctn)(node);

	if (hasLeft(node))
		preOrder(node->left, fctn);
	
	if (hasRight(node))
		preOrder(node->right, fctn);
}

void inOrder(TreeNode * node, void (*fctn)(TreeNode*)) {
	if (!node) return;

	if (hasLeft(node))
		inOrder(node->left, fctn);
	
	(*fctn)(node);
	
	if (hasRight(node))
		inOrder(node->right, fctn);
}

TreeNode* insert(TreeNode ** node, int valueToAdd) {
	TreeNode* added;
	
	if (!(*node)) {
		*node = (TreeNode*) malloc(sizeof(TreeNode));
		(*node)->value = valueToAdd;
		(*node)->left = (*node)->right = 0;
		(*node)->parent = 0;		
		return *node;
	} else if (valueToAdd < (*node)->value) {
		added = insert( (TreeNode**) &(*node)->left, valueToAdd);
	} else if (valueToAdd > (*node)->value) {
		added = insert( (TreeNode**) &(*node)->right, valueToAdd);
	}
	
	if ( (*node)->left == added || (*node)->right == added ) {
		added->parent = *node;
	}
	
	return added;
}

TreeNode * goRight(TreeNode * node) {
	if (hasRight(node))
		return goRight(node->right);
	return node;
}

TreeNode * goLeft(TreeNode * node) {
	if (hasLeft(node))
		return goLeft(node->left);
	return node;
}

void removeNode(TreeNode * node) {
	int n = numberOfChildren(node);
	TreeNode * parent = node->parent;
	TreeNode * temp;

	if (n == 2) {
		temp = goLeft(node->right);
		printf("using %d to swap\n", temp->value);
		if (node->left!=temp) {
			temp->left = node->left;
			((TreeNode*) (node->left))->parent = temp;
		} else {
			temp->left = 0;
		}
		if (node->right!=temp) {
			temp->right = node->right;
			((TreeNode*) (node->left))->parent = temp;
		} else {
			temp->right = NULL;
		}
		
		TreeNode * par;
		if (node->parent!=0) {
			par = node->parent;
			if (par->left == node)
				par->left = temp;
			else
				par->right = temp;
		}
		
		par = temp->parent;
		if (par->left == temp)
			par->left = NULL;
		else
			par->right = NULL;
	
		temp->parent = node->parent;

	} else if (n==1) {
		if (hasLeft(node))
			temp = node->left;
		else
			temp = node->right;
		
		if (hasLeft(parent)) {
			parent->left = temp;
		} else {
			parent->right = temp;
		}
	} else {
		if (parent->right == node) {
			parent->right = NULL;
		} else {
			parent->left = NULL;
		}
	}
	
	free(node);
}
