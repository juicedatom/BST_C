#ifndef TREENODE_H
#define TREENODE_H

//struct which olds all node information
typedef struct {
	void * parent;
	void * left;
	void * right;
	int value;
} TreeNode;

//they do what they sound like they do
int hasLeft(TreeNode*);
int hasRight(TreeNode*);
int numberOfChildren(TreeNode*);

//these functions will move through the tree and
//purform the function given to them by their respective
//orderings
void inOrder(TreeNode*,void (*)(TreeNode*));
void preOrder(TreeNode*, void (*)(TreeNode*));
void postOrder(TreeNode*, void (*)(TreeNode*));

TreeNode* goRight(TreeNode*);
TreeNode* goLeft(TreeNode*);

//creates a new tree node
void destroyTree(TreeNode*);

TreeNode* insert(TreeNode**, int);
void removeNode(TreeNode*);

#endif
