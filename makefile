CC=gcc
CFLAGS=-c -Wall
LDFLAGS=
SOURCES=main.c TreeNode.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=TreeTest

all: $(SOURCES) $(EXECUTABLE)
	
$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.c.o:
	$(CC) $(CFLAGS) $< -o $@
